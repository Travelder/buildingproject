﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using TMPro;
using Deform;
public class FloorManager : MonoBehaviour
{
    public GameObject floor;
    private float multiplier = 0.538f;

    private float tilt = 2f;
    // private float yAdjust = -0.29f;
    private float xAdjust = -0.0485f;

    private float xAdder = -0.01934f;
    // private float xFloorAdjust = -.008f;

    private int tiltCount = 0;
    private float tiltAngle = 0f;
    private int tiltedFloors = 0;
    private bool repeatSpawn = false;
    private Vector3 initialPosition;
    private Quaternion initialRotation;

    private List<GameObject> floorList = new List<GameObject>();

    // Start is called before the first frame update

    public void DataReceived(string data)
    {
        var result = JsonConvert.DeserializeObject<Result>(data);
        if (result == null)
        {
            result = new Result();
            result.categories = new List<CategoryModel>();
            for (int j = 0; j < 4; j++)
            {
                var cat_model = new CategoryModel()
                {
                    category = new CategoryDetail()
                    {
                        id = j,
                        name = "catname" + j.ToString()
                    },
                    avg_points = 5,
                    recommendation = new Recommendation()
                    {
                        min_range = 1,
                        max_range = 10,
                        title = "recommendation" + j.ToString(),
                        description = "recommendation" + j.ToString(),
                        tilt_angle = j,
                    },
                };
                result.categories.Add(cat_model);

            }
        }
        // result.categories.Add(result.categories[0]);
        foreach (var floorItem in floorList)
        {
            Destroy(floorItem);
        }
        tiltCount = 0;
        tiltAngle = 0f;
        tiltedFloors = 0;
        int i = 0;
        for (i = 0; i < result?.categories.Count; i++)
        {
            SpawnFloor(result.categories[i], i);
        }
        Transform roof = gameObject.transform.Find("RoofPivot");
        if (!repeatSpawn)
        {
            initialPosition = roof.position;
            initialRotation = roof.rotation;
            repeatSpawn = true;
        }
        else
        {
            roof.position = initialPosition;
            roof.rotation = initialRotation;
        }
        roof.Translate(Vector3.up * this.multiplier * i);
        // roof.position += Vector3.up * multiplier * i;
        // roof.position += Vector3.up * 0.15f;
        if (tiltCount > 0)
        {
            var offset = 0;
            if (result.categories.Count > 0)
            {
                offset = (result.categories.Count - tiltCount);
            }
            var x = (xAdder + 0.02f * tiltCount) - (xAdjust * tiltedFloors);
            var positionChange = new Vector3(x, 0, 0);
            roof.Translate(positionChange);
            roof.Rotate(Vector3.forward, tiltAngle * -1);
        }
    }

    public void SpawnFloor(CategoryModel category, int floorNo)
    {
        var new_floor = Instantiate(floor, gameObject.transform);
        floorList.Add(new_floor);
        var increased_height = floorNo * this.multiplier;
        new_floor.transform.Translate(Vector3.up * increased_height, Space.Self);

        AdjustFloor(category, new_floor, floorNo);
    }
    public void AdjustFloor(CategoryModel category, GameObject floor, int floorNo)
    {
        print("adjusting floor " + floorNo.ToString());
        var floor_parent = GetChildWithName(floor, "Floor");
        var floor_object = GetChildWithName(floor_parent, "floor");
        var deform = GetChildWithName(floor_object, "Bend");
        var bend = deform.GetComponent<BendDeformer>();
        if (tiltCount > 0)
        {
            print("Moving");
            int offset = tiltedFloors / 3;

            var x = ((xAdder - (offset * 0.0015f)) * tiltCount) + (xAdjust * tiltedFloors);
            var positionChange = new Vector3(x, 0, 0);
            print(positionChange);
            floor.transform.Translate(positionChange);
            floor.transform.Rotate(Vector3.forward, tiltAngle);
            tiltedFloors++;
        }
        // float floor_tilt = tilt * 1;
        // bend.Angle = floor_tilt;
        // tiltCount += 1;
        // tiltAngle += floor_tilt;
        if (category.recommendation.tilt_angle > 0)
        {
            float floor_tilt = tilt * category.recommendation.tilt_angle;
            bend.Angle = floor_tilt;
            tiltCount += category.recommendation.tilt_angle;
            tiltAngle += floor_tilt;
        }
        print(bend.Angle);

        GameObject label = GetChildWithName(floor_object, "Label");
        var textMeshPro = label.GetComponent<TextMeshPro>();
        if (textMeshPro != null)
        {
            textMeshPro.text = category.category.name;
            // textMeshPro.text = "Text";
        }
    }

    GameObject GetChildWithName(GameObject obj, string name)
    {
        Transform trans = obj.transform;
        Transform childTrans = trans.Find(name);
        if (childTrans != null)
        {
            return childTrans.gameObject;
        }
        else
        {
            return null;
        }
    }
}
