public class Recommendation {
    public int min_range;
    public int max_range;
    public string title;
    public string description;
    public int tilt_angle;
}