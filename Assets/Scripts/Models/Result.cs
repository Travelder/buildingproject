using System;
using System.Collections;
using System.Collections.Generic;
public class Result {
    public int id;
    public Project project;
    public List<CategoryModel> categories;
}