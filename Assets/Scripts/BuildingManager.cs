﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BuildingManager : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("FullBuilding").SendMessage("DataReceived", "");
    }

    // Update is called once per frame
    // void Update()
    // {
        
    // }
    void Awake()
    {
        UnityMessageManager.Instance.OnRNMessage += onMessage;
    }

    void onDestroy()
    {
        UnityMessageManager.Instance.OnRNMessage -= onMessage;
    }
    void onMessage(MessageHandler message)
    {
        var data = message.getData<string>();
        Debug.Log("onMessage:" + data);
        // canRotate = !canRotate;
        message.send(new { CallbackTest = message });
        GameObject.Find("FullBuilding").SendMessage("DataReceived", data);
    }
}
